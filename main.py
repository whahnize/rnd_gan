import argparse
import numpy as np
import tensorlayer as tl
from tensorlayer.layers import *
import tensorflow as tf
import time
import os
from model import *
from datetime import datetime
from tensorflow.contrib import layers
from random import shuffle
from glob import glob

parser = argparse.ArgumentParser()
parser.add_argument("--epoch", type=int, default=10, help="Epoch to train [5]")
parser.add_argument("--learning_rate", type=float, default=0.0001, help="Learning rate of adam [0.001]")
parser.add_argument("--batch_size", type=int, default=50, help="The number of batch images [64]")
parser.add_argument("--data_dir", default='D:/data/rnd_gan', help="The directory of h5 files")
parser.add_argument("--log_dir", default="log", help="The directory for logging")
parser.add_argument("--checkpoint_dir", default="npz", help="Directory name to save the checkpoints [checkpoint]")
parser.add_argument("--img_resolution", type=int, default=64, help="The resolution of images")
parser.add_argument("--print_freq", type=int, default=1, help="The interval of batches to print")
parser.add_argument("--save_interval", type=int, default=10, help="The interval of saving npz")
parser.add_argument('--mode', default='train', help='mode [train, test]')

# Arguments for loading 
parser.add_argument("--load_time", default=None, help="load time")
parser.add_argument("--max_acc", type=float, default=0.7578, help="used when load the pretrained model")
parser.add_argument("--npz_to_load", default='18-06-23(11h47m)_0.7578_0948_net.npz', help="used when load the pretrained model")
FLAGS = parser.parse_args()

BATCH_SIZE = FLAGS.batch_size
NUM_EPOCH = FLAGS.epoch
LEARNING_RATE = FLAGS.learning_rate
DATA_DIR = FLAGS.data_dir
RESOLUTION = FLAGS.img_resolution
CHECKPOINT_DIR = FLAGS.checkpoint_dir
LOG_DIR = FLAGS.log_dir
PRINT_FREQ = FLAGS.print_freq
LOAD_TIME = FLAGS.load_time
MAX_ACC = FLAGS.max_acc
MODE = FLAGS.mode
NPZ_TO_LOAD = FLAGS.npz_to_load
SAVE_INTERVAL = FLAGS.save_interval

if LOAD_TIME is not None: now = LOAD_TIME
else: now = datetime.now().strftime('%y-%m-%d(%Hh%Mm)')

LOG_FOUT = open(os.path.join(LOG_DIR, now + '_log_train.txt'), 'a')
LOG_FOUT.write(str(FLAGS)+'\n')
LOG_FOUT.write(datetime.now().strftime('%y-%m-%d(%Hh%Mm)')+'\n')

tl.files.exists_or_mkdir(CHECKPOINT_DIR)
tl.files.exists_or_mkdir(LOG_DIR)

LOG_FOUT = open(os.path.join(LOG_DIR, now + '_log_train.txt'), 'a')
LOG_FOUT.write('****' + datetime.now().strftime('%y-%m-%d(%Hh%Mm)') + '****' + '\n')
LOG_FOUT.write(str(FLAGS)+'\n\n')

def log_string(out_str):
    LOG_FOUT.write(out_str+'\n')
    LOG_FOUT.flush()
    print(out_str)

sess = tf.InteractiveSession()

# define placeholder
x = tf.placeholder(tf.float32, shape=[None, RESOLUTION, RESOLUTION, 3], name='x') 
y_ = tf.placeholder(tf.int64, shape=[None], name='y_')
    
# define inferences
net_train = model(x, is_train=True, reuse=False)
net_test = model(x, is_train=False, reuse=True)

# define cost function and metric.
y = net_train.outputs
cost = tl.cost.cross_entropy(y, y_, name='xentropy') 

n_params = len(net_train.all_params)
for i in range(n_params):
    cost = cost + tf.contrib.layers.l2_regularizer(0.01/n_params)(net_train.all_params[i])

# cost and accuracy for evaluation
y2 = net_test.outputs
cost_test = tl.cost.cross_entropy(y2, y_, name='xentropy2')
correct_prediction = tf.equal(tf.argmax(y2, 1), y_)
acc = tf.reduce_mean(tf.cast(correct_prediction, LayersConfig.tf_dtype))

# define the optimizer
global_step = tf.Variable(0, trainable=False)
learning_rate = tf.train.exponential_decay(LEARNING_RATE, global_step, 1000, 0.95, staircase=True)
train_params = net_train.all_params
train_op = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE).minimize(cost, var_list=train_params, global_step=global_step)

# initialize all variables in the session
tl.layers.initialize_global_variables(sess)

# print network information
log_string('NETWORK VARIABLES\n')
log_string(str(net_train.all_params)+'\n')
log_string('NETWORK LAYERS\n')
log_string(str(net_train.all_layers))
log_string('------------------------')

TRAIN_DIR = os.path.join(DATA_DIR, 'train')
VAL_DIR = os.path.join(DATA_DIR, 'val')
TEST_DIR = os.path.join(DATA_DIR, 'samples')

train_r_files = glob(os.path.join(TRAIN_DIR, '01_*', '*.*'))
train_g_files = glob(os.path.join(TRAIN_DIR, '00_*', "*.*"))

val_r_files = glob(os.path.join(VAL_DIR, 'real', "*.*"))
val_g_files = glob(os.path.join(VAL_DIR, 'generated', "*.*"))

test_r_files = glob(os.path.join(TEST_DIR, 'real', "*.jpg"))
test_g_files = glob(os.path.join(TEST_DIR, 'generated', "*.jpg"))
num_batch_t = min(len(train_r_files), len(train_g_files)) // BATCH_SIZE*2

num_iter = 0
min_err = np.inf

if LOAD_TIME is not None:
    tl.files.load_and_assign_npz(sess=sess, name=os.path.join(CHECKPOINT_DIR, NPZ_TO_LOAD), network=net_train)
    num_iter = int(NPZ_TO_LOAD.split('_')[-2])

for epoch in range(num_iter // num_batch_t, NUM_EPOCH):

    shuffle(train_r_files)
    shuffle(train_g_files)

    for t_idx in range(0, num_batch_t):
        start_time = time.time()

        # load training data
        train_r_images = tl.visualize.read_images(train_r_files[t_idx*BATCH_SIZE//2:(t_idx+1)*BATCH_SIZE//2], printable=False)
        # Random zoom
        # train_r_images = tl.prepro.threading_data(train_r_images, tl.prepro.imresize, size=[RESOLUTION*2, RESOLUTION*2])
        # train_r_images = tl.prepro.threading_data(train_r_images, tl.prepro.zoom, zoom_range=[1., 1.1], is_random=True)
        # train_r_images = tl.prepro.threading_data(train_r_images, tl.prepro.crop, wrg = RESOLUTION, hrg = RESOLUTION)
        train_r_images = tl.prepro.threading_data(train_r_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])

        train_g_images = tl.visualize.read_images(train_g_files[t_idx*BATCH_SIZE//2:(t_idx+1)*BATCH_SIZE//2], printable=False)
        train_g_images = tl.prepro.threading_data(train_g_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])

        train_r_labels = np.zeros(BATCH_SIZE//2) 
        train_g_labels = np.ones(BATCH_SIZE//2) 

        indices = np.arange(BATCH_SIZE)

        np.random.shuffle(indices)
        X_train = np.concatenate([train_r_images, train_g_images])[indices]
        Y_train = np.concatenate([train_r_labels, train_g_labels])[indices]
        
        # load validation data
        batch_files = np.random.choice(val_r_files, BATCH_SIZE//2)
        val_r_images = tl.visualize.read_images(batch_files, printable=False)
        val_r_images = tl.prepro.threading_data(val_r_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])

        batch_files = np.random.choice(val_g_files, BATCH_SIZE//2)
        val_g_images = tl.visualize.read_images(batch_files, printable=False)
        val_g_images = tl.prepro.threading_data(val_g_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])     

        val_r_labels = np.zeros(BATCH_SIZE//2) 
        val_g_labels = np.ones(BATCH_SIZE//2)

        indices = np.arange(BATCH_SIZE)
        np.random.shuffle(indices)
        
        X_val = np.concatenate([val_r_images, val_g_images])[indices]
        Y_val = np.concatenate([val_r_labels, val_g_labels])[indices]

        err, ac, _ = sess.run([cost_test, acc, train_op], feed_dict={x: X_train, y_: Y_train})
        val_err, val_ac = sess.run([cost_test, acc], feed_dict={x: X_val, y_: Y_val})

        if num_iter % PRINT_FREQ == 0 and num_iter > 0:
            log_string("Epoch: [%2d/%2d] [%4d/%4d] (iter.: %5d) time: %4.4f, tr_loss: %.4f, tr_acc: %.2f, val_loss: %.4f, val_acc: %.2f"
                        % (epoch, NUM_EPOCH, t_idx, num_batch_t, num_iter, time.time() - start_time, err, ac*100, val_err, val_ac*100)) 

        num_iter += 1

    # Validation at the end of every epoch
    val_idxs = min(len(val_r_files), len(val_g_files)) // BATCH_SIZE*2
    val_loss, val_acc, num_batch_v = 0, 0, 0
    for v_idx in range(0, val_idxs):
        val_r_images = tl.visualize.read_images(val_r_files[v_idx*BATCH_SIZE//2:(v_idx+1)*BATCH_SIZE//2], printable=False)
        val_r_images = tl.prepro.threading_data(val_r_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])
        val_g_images = tl.visualize.read_images(val_g_files[v_idx*BATCH_SIZE//2:(v_idx+1)*BATCH_SIZE//2], printable=False)
        val_g_images = tl.prepro.threading_data(val_g_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])

        val_r_labels = np.zeros(BATCH_SIZE//2) 
        val_g_labels = np.ones(BATCH_SIZE//2) 
        X_val = np.concatenate([val_r_images, val_g_images])
        Y_val = np.concatenate([val_r_labels, val_g_labels])
        
        err, ac = sess.run([cost_test, acc], feed_dict={x: X_val, y_: Y_val})

        val_loss += err
        val_acc += ac
        num_batch_v += 1
    
    val_loss, val_acc = val_loss / num_batch_v, val_acc / num_batch_v
    log_string("[*] End of Epoch: [%2d/%2d] time: %4.4f, val_loss: %.4f, val_acc: %.2f" % (epoch, NUM_EPOCH, time.time() - start_time, val_loss, val_acc*100))

    # after "half" epochs, save model
    if val_loss < min_err and num_iter > NUM_EPOCH * num_batch_t // 2:
        min_err = val_loss
        tl.files.save_npz(net_train.all_params, name=os.path.join(CHECKPOINT_DIR, now + '_' + str(epoch) + '_' + str(num_iter) + '_minErr.npz'), sess=sess)
        log_string("[*] Minimum validation error updated [%.4f]" % min_err)
        log_string("[*] Saving [%d epoch] checkpoints SUCCESS!" % epoch)

    if epoch % SAVE_INTERVAL == 0 and epoch is not 0:
        tl.files.save_npz(net_train.all_params, name=os.path.join(CHECKPOINT_DIR, now + '_' + str(epoch) + '_' + str(num_iter) + '_net.npz'), sess=sess)
        log_string("[*] Saving [%d epoch] checkpoints SUCCESS!" % epoch)

# Evaluation
path = glob(os.path.join(CHECKPOINT_DIR, now+"*minErr*"))
if len(path) != 0:
    path = path[-1]
    tl.files.load_and_assign_npz(sess=sess, name=path, network=net_train)
    log_string("[*] Loading min-error model SUCCESS! [%s]" % os.path.basename(path))
else:
    log_string("[*] Cannot load model!")

test_r_loss, test_g_loss = 0, 0
test_r_acc, test_g_acc = 0, 0
num_batch = min(len(test_r_files), len(test_g_files)) // BATCH_SIZE
for s_idx in range(num_batch):
    test_r_images = tl.visualize.read_images(test_r_files[s_idx*BATCH_SIZE:(s_idx+1)*BATCH_SIZE], printable=False)
    test_r_images = tl.prepro.threading_data(test_r_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])
    test_g_images = tl.visualize.read_images(test_g_files[s_idx*BATCH_SIZE:(s_idx+1)*BATCH_SIZE], printable=False)
    test_g_images = tl.prepro.threading_data(test_g_images, tl.prepro.imresize, size=[RESOLUTION, RESOLUTION])

    X_test = np.array(test_r_images)
    Y_test = np.zeros(BATCH_SIZE)

    err, ac = sess.run([cost_test, acc], feed_dict={x: X_test, y_: Y_test})
    test_r_loss += err
    test_r_acc += ac

    X_test = np.array(test_g_images)
    Y_test = np.ones(BATCH_SIZE)

    err, ac = sess.run([cost_test, acc], feed_dict={x: X_test, y_: Y_test})
    test_g_loss += err
    test_g_acc += ac

if num_batch > 0:
    test_r_loss /= num_batch
    test_g_loss /= num_batch
    test_r_acc /= num_batch
    test_g_acc /= num_batch
    test_total_loss = (test_r_loss + test_g_loss) / 2
    test_total_acc = (test_r_acc + test_g_acc) / 2
    log_string("[*] TEST loss: %.4f, acc: %.2f, r_loss: %.4f, r_acc: %.2f, g_loss: %.4f, g_acc: %.2f"
               % (test_total_loss, test_total_acc*100, test_r_loss, test_r_acc*100, test_g_loss, test_g_acc*100))